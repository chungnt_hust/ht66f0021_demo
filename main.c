#include "HT66F0021.h"
#include "Interrupt.h"
#include "SystemClk.h"
#include "Timer.h"
#include "PWM.h"
#include "EmulatedEEPROM.h"
#include "ADC.h"

extern volatile u16 system_tick;

extern volatile u8 flag1ms;

#define SET_LED_PIN_AS_GPIO {_pas6 = 0; _pas5 = 0; _pwmen = 0;}
#define SET_LED_PIN_AS_PWM  {_pas6 = 0;	_pas5 = 1; _pwmen = 1;}

#define LED_PIN _pa7

#define LED_ON 		(LED_PIN = 1)
#define LED_OFF 	(LED_PIN = 0)
#define LED_TOGGLE 	(LED_PIN = !LED_PIN)

#define STOP_BTN _pa5
#define STOP_IDLE_STATE   0
#define STOP_ACTIVE_STATE 1

u8 Status_Demi, Status_Stop;
u8 count = 0;
u16 timeout;

typedef enum
{
	BTN_STATE_IDLE = 0,
	BTN_STATE_PRESS,
	BTN_STATE_RELEASE,
} btnState_t;

btnState_t btnState = BTN_STATE_RELEASE;
// u8 btnActiveLevel;
// bool isDimUp = FALSE;



// void testCode(void)
// {
// 	static u8 count = 0;
// 	if(++count >= 100)
// 	{
// 		count = 0;
// 		_pa7 = !_pa7;
// 	}
	
// }

void demEffect(void)
{
	switch(Status_Demi)
	{
		case 1:
		{
			LED_ON;
			_timeDelay = 40;
			while((STOP_BTN == STOP_IDLE_STATE) && (_timeDelay > 0));
			LED_OFF;
			_timeDelay = 80;
			while((STOP_BTN == STOP_IDLE_STATE) && (_timeDelay > 0));
			break;
		}			
		case 2:
		{	
			// for(count = 0; ; count++)
			// {
			// 	if(count == 255) isDimUp = !isDimUp;
			// 	if(isDimUp == TRUE) _pwmdata = count;
			// 	else _pwmdata = ~count;
			// 	_timeDelay = 3;
			// 	while(_timeDelay > 0);
			// }
			for(count = 0; count < 255; count++)
			{
				_pwmdata = count;
				_timeDelay = 3;
				while(_timeDelay > 0);
				if(STOP_BTN == STOP_ACTIVE_STATE) break;
				// while((STOP_BTN == STOP_IDLE_STATE) && (_timeDelay > 0));
			}
			for(count = 255; count > 0; count--)
			{
				_pwmdata = count;
				_timeDelay = 3;
				while(_timeDelay > 0);
				if(STOP_BTN == STOP_ACTIVE_STATE) break;
				// while((STOP_BTN == STOP_IDLE_STATE) && (_timeDelay > 0));
			}
			break;
		}				
		case 3:
		{	
			LED_OFF;
			_timeDelay = 400;
			while((STOP_BTN == STOP_IDLE_STATE) && (_timeDelay > 0));
			for(count = 0; count < 5; count++)
			{ 
				LED_TOGGLE;
				_timeDelay = 40;
				while((STOP_BTN == STOP_IDLE_STATE) && (_timeDelay > 0));
			}
			break;
		}		
		case 4:	
		default: 		
		{	
			LED_ON;
			break;
		} 
	}	
}

void stopEffect(void)
{
    switch(Status_Stop)
    {
    	case 1:
		{
    		LED_ON;
			// LED_TOGGLE;
			// _timeDelay = 1000;
			// while((STOP_BTN == STOP_ACTIVE_STATE) && (_timeDelay > 0));
    		break;
		}
    	case 3:
		{
			LED_OFF;
			_timeDelay = 400;
			while((STOP_BTN == STOP_ACTIVE_STATE) && (_timeDelay > 0));
			for(count = 0; count < 5; count++)
			{ 
				LED_TOGGLE;
				_timeDelay = 40;
				while((STOP_BTN == STOP_ACTIVE_STATE) && (_timeDelay > 0));
			}
			break;
		}
    	case 4:
		{
			LED_OFF;
			_timeDelay = 160;
			while((STOP_BTN == STOP_ACTIVE_STATE) && (_timeDelay > 0));
			LED_OFF;
			_timeDelay = 80;
			while((STOP_BTN == STOP_ACTIVE_STATE) && (_timeDelay > 0));
			break;	
		}
    	case 5:
		{
			LED_OFF;
			_timeDelay = 80;
			while((STOP_BTN == STOP_ACTIVE_STATE) && (_timeDelay > 0));
			LED_OFF;
			_timeDelay = 40;
			while((STOP_BTN == STOP_ACTIVE_STATE) && (_timeDelay > 0));
			break;	
		}	  
    	case 6:
		{
			LED_OFF;
			_timeDelay = 50;
			while((STOP_BTN == STOP_ACTIVE_STATE) && (_timeDelay > 0));
			LED_OFF;
			_timeDelay = 25;
			while((STOP_BTN == STOP_ACTIVE_STATE) && (_timeDelay > 0));
			break;	
		}		
		case 2:
		case 7:
    	default:
		{
			LED_OFF;
			_timeDelay = 400;
			while((STOP_BTN == STOP_ACTIVE_STATE) && (_timeDelay > 0));
			for(count = 0; count < 3; count++)
			{ 
				LED_TOGGLE;
				_timeDelay = 40;
				while((STOP_BTN == STOP_ACTIVE_STATE) && (_timeDelay > 0));
			}
			break;	
		}
    }	
}

u16 timePress, totalPressTime;
u8 countReleaseAction = 0;
u16 timeCheckMultiRelease = 0;
u8 btnIsChecking = FALSE;
u8 firstPwron = TRUE;

void checkBtn(void)
{
	if(flag1ms)
	{
		flag1ms = 0;
		// btnActiveLevel = STOP_BTN;

		// totalPressTime = system_tick - timePress;
		switch(btnState)
		{
			case BTN_STATE_RELEASE:
			{
				if(STOP_BTN == STOP_ACTIVE_STATE)
				{
					timePress = system_tick;
					btnState = BTN_STATE_PRESS;
				}
				break;
			}
			case BTN_STATE_PRESS:
			{
				if(STOP_BTN == STOP_IDLE_STATE)
				{
					btnState = BTN_STATE_RELEASE;
					totalPressTime = system_tick - timePress;
					if(totalPressTime < 500) // < 500ms moi la nhan nha
					{
						timeCheckMultiRelease = 1000;
						countReleaseAction++;
						if(countReleaseAction > 0) btnIsChecking = TRUE;
						// if(countReleaseAction == 0)
						// {
						// 	timeCheckMultiRelease = system_tick;
						// }
						// if((system_tick - timeCheckMultiRelease) < 1000) // 2 lan nha phim khong duoc > 1s
						// {
						// 	if(++countReleaseAction >= 6) 
						// 	{
						// 		countReleaseAction = 0;
						// 		if(Status_Stop < 9)
						// 		{
						// 			Status_Stop++;
						// 			if(Status_Stop == 9) Status_Stop = 1;
						// 		}
						// 	}
						// }
						// else
						// {
						// 	countReleaseAction = 0;
						// }
					}
				}
				break;
			}
		}

		if(timeCheckMultiRelease > 0)
		{
			if(--timeCheckMultiRelease == 0)
			{
				switch(countReleaseAction)
				{
					case 6:
					{
						countReleaseAction = 0;

						if(firstPwron == TRUE)
						{
							firstPwron = FALSE;
							if(Status_Demi < 4)
							{
								Status_Demi++;
								if(Status_Demi == 4) Status_Demi = 1;
							}
						}
						else
						{
							if(Status_Stop < 9)
							{
								Status_Stop++;
								if(Status_Stop == 9) Status_Stop = 1;
							}
						}
						break;

						EmulatedEEPROM_Erase();
						EmulatedEEPROM_Write_Byte(0x00, Status_Demi);
						EmulatedEEPROM_Write_Byte(0x01, Status_Stop);
					}
					default: { countReleaseAction = 0; break; }
				}

				btnIsChecking = FALSE;
			}
		}
	}
}

void main()
{
	SysClock_Init();
	TM_Init();
	PWM_Init();	
	ADC_Init();
	
	_pac7 	= 0; 	// PA7 output
	_pac5   = 1;	// PA1 input
	
	Status_Demi = EmulatedEEPROM_Read_Byte(0x00); 
	Status_Stop = EmulatedEEPROM_Read_Byte(0x01); 
	if(Status_Demi == 0) Status_Demi = 1;
	if(Status_Stop == 0) Status_Stop = 1;

	timeout = system_tick;

	while(system_tick - timeout < 15000)
	{
		checkBtn();
		if(firstPwron == FALSE) break;
	}

	firstPwron = FALSE;

	while(1)
	{		

		checkBtn();

		if(btnIsChecking == FALSE)
		{
			if(STOP_BTN == STOP_IDLE_STATE) demEffect();
			else stopEffect();
		}
	}
}
//if(flag1ms == 1) // set flag1ms trong ngat timer 1ms
//		{
//			flag1ms = 0;
//			flag10ms++;
//			
//		}
//		if(flag10ms >= 10)
//		{
//			flag10ms = 0;
//			flag100ms++;
//			
//		}
//		if(flag100ms >= 10)
//		{
//			flag100ms = 0;
//			flag1s++;
//			
//		}
//		if(flag1s >= 10)
//		{
//			flag1s = 0;
//			
//		}