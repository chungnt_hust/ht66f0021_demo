/*
 * HT8_EmulatedEEPROM.h
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
 *
 *      
*/

/* Define to prevent recursive inclusion -------------------------------------*/

#ifndef _HT8_EEPROM_H_
#define	_HT8_EEPROM_H_

#include "HT66F0021.h"
#include "HT8_Type.h"

void EmulatedEEPROM_Erase(void);
void EmulatedEEPROM_Write_Byte(u8 adr, u8 Data);				//EEPROM Write mode,No interrupt
u8 EmulatedEEPROM_Read_Byte(u8 adr);							//EEPROM Read mode

#endif

/******************* (C) COPYRIGHT 2018 Holtek Semiconductor Inc *****END OF FILE****/