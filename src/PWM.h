/*
 * HT8_PWM.h
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
 *
 *      
*/

/* Define to prevent recursive inclusion -------------------------------------*/

#ifndef _PWM_H_
#define _PWM_H_

#include "HT66F0021.h"
#include "HT8_Type.h"

void PWM_Init(void);

#endif
/******************* (C) COPYRIGHT 2018 Holtek Semiconductor Inc *****END OF FILE****/