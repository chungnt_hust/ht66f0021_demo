/*
 * PWM.c
 *
 *  Created on: May 18, 2021
 *      Author: chungnt@epi-tech.com.vn
*/
/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "PWM.h"

void PWM_Init(void)
{
	// Fsys = 8MHz/64
	// Fdiv = 8/64/4 = 31.25kHz
	// fre  = Fdiv/256 = 122Hz
	_pwmdiv0 = 1;
	_pwmdiv1 = 1;	
	_pwmsel = 1; // 7+1 mode 	fre  = Fdiv/128 = 250Hz
//	_pas6 = 0;	_pas5 = 1;// PA7 as PWM0
/*	_pas4 = 1;// PA6 as PWM0*/

}

