/*
 * HT8_it.h
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
 *
 *      
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _HT8_it_h_
#define _HT8_it_h_
#include "HT66F0021.h"
#include "HT8_Type.h"

extern volatile u16 _timeDelay;
void delayMs(u16 timeDelay);

#endif

/******************* (C) COPYRIGHT 2018 Holtek Semiconductor Inc *****END OF FILE****/