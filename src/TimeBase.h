/*
 * HT8_TimeBase.h
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
 *
 *      
*/

/* Define to prevent recursive inclusion -------------------------------------*/


#ifndef _HT8_TimeBase_H_
#define _HT8_TimeBase_H_

#include "HT66F0021.h"

/** TimeBase initialization function,you can select 
  * TimeBase clock,TimeBase0 time-out period and TimeBase1 time-out period
  * in TimeBase.h.
  */
void TimeBase_Init(void);


#endif//end of _HT8_TimeBase_H_

/******************* (C) COPYRIGHT 2018 Holtek Semiconductor Inc *****END OF FILE****/