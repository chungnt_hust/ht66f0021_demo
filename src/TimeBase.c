/*
 * TimeBase.c
 *
 *  Created on: May 18, 2021
 *      Author: chungnt@epi-tech.com.vn
*/
/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "TimeBase.h"

/**
  * @brief Initializes the TimeBase peripheral according to the specified parameters
  * @retval None
  */
void TimeBase_Init(void)
{
	_pscen = 0; //  Prescaler control disable
/***************** TimeBase clock select *****************/
	_clksel1 = 1; _clksel0 = 0; // Prescaler clock source fPSC selection = fSUB = 32kHz
/*************** end of TimeBase clock select ******************/	

						
/******** TimeBase time-out period select**********/
	/* select TimeBase time-out period 2^8(256)	*/
	_tb2 = 1;	_tb1 = 0;	_tb0 = 0; 
/***** end of TimeBase time-out period select ******/

	_tbe = 1; // enable timebase 0 interrupt
	_tbf = 1; // set flag to 1
	_tbon = 1; // enable timebase

}
