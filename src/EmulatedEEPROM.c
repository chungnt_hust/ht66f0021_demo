/*
 * EmulatedEEPROM.c
 *
 *  Created on: May 18, 2021
 *      Author: chungnt@epi-tech.com.vn
*/
/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "EmulatedEEPROM.h"

/* Erase 1 page = 16words */
void EmulatedEEPROM_Erase()
{
	/* Erasing a Data Page of the Emulated EEPROM */
	_ear = 0x00;	
	_ecr = 0x00;		// Erase time=2ms (40H for 4ms, 80H for 8ms, C0H for 16ms)
	_emi = 0;
	_eeren = 1;			// set EEREN bit,enable	erase operation
	_eer = 1;			// start Erase Cycle, set	EER	bit, executed immediately after setting EEREN bit
	_emi = 1;
	while(_eer == 1){_clrwdt();}	// check for erase cycle end	
}

/**
  * @brief EmulatedEEPROM write function.
  * @param[in] The data you want to write to EmulatedEEPROM.
  * It can be 0x00~0xff.
  * @param[in] Specifies EmulatedEEPROM address.
  * It can be 0x00~0x1f.
  * @retval None
  * 2page emulated eeprom
  *	Erase 16 words/page
  *	Write 1 word/time
  * Read 1 word/time
  * Page size = 16 words.
  */
void EmulatedEEPROM_Write_Byte(u8 adr, u8 Data)
{	
	/* Writing Data to the Emulated EEPROM */
	_ear = adr; 		// user defined address
	_edl = Data;
	_edh = 0;
	
	_ecr = 0x00;		// Write time=2ms (40H for 4ms, 80H for 8ms, C0H for 16ms)
	_emi = 0;				
	_ewren = 1;			// set EWREN bit,enable	write operation
	_ewr = 1;			// start Write Cycle, set EWR bit, executed immediately after setting EWREN bit	
	_emi = 1;
	while(_ewr==1){_clrwdt();}	// check for write cycle end
}
	

/**
  * @brief EmulatedEEPROM read function.
  * @param[in] Specifies EmulatedEEPROM address that you want to read.
  * It can be 0x00~0x1f.
  * @retval EEPROM data.
  */
u8 EmulatedEEPROM_Read_Byte(u8 adr)
{
	u8 eepromData = 0xFF;
	u8 temp;
	
	_ear = adr;					// user	defined	address
	_erden = 1;					// set ERDEN bit, enable read operation
	_erd = 1;					// start Read Cycle -set ERD bit
	while(_erd==1){_clrwdt();}			// check for read cycle end
	_ecr = 0;					// disable Emulated	EEPROM read	if no more read	
        						// operations are required       									 	 
	eepromData = _edl;			// move	read data to register
	temp = _edh;
		
	return eepromData;
}

/******************* (C) COPYRIGHT 2018 Holtek Semiconductor Inc *****END OF FILE****/