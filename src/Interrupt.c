/*
 * Interrupt.c
 *
 *  Created on: May 18, 2021
 *      Author: chungnt@epi-tech.com.vn
*/
/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "Interrupt.h"
#include "Timer.h"

volatile u16 _timeDelay = 0;
volatile u16 system_tick = 0;
volatile u8 flag1ms = 0;
void delayMs(u16 timeDelay)
{
	_timeDelay = timeDelay;
	while(_timeDelay > 0); //_clrwdt();
}

/**
  * @brief timer periodic interrupt routine.
  * @par Parameters:
  * None
  * @retval
  * None
  */
void __attribute((interrupt(0x0C))) PTM_ISR(void)
{
	 _clrwdt();
	/* user define */
	_tf = 0;
	system_tick++;
	flag1ms = 1;
	if(_timeDelay > 0) _timeDelay--;
	_emi = 1; // enable global interrupt
}

/**
  * @brief timebase Interruption routine.
  * @par Parameters:
  * None
  * @retval
  * None
  */
void __attribute((interrupt(0x08))) TB_ISR(void)
{
	/* user define */
	_tbf = 0;

}


/**
  * @brief external Interruption 1 routine.
  * @par Parameters:
  * None
  * @retval
  * None
  */
//void __attribute((interrupt(0x24))) INT1_ISR(void)
//{
//	/* user define */
//}
