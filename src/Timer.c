/*
 * Timer.c
 *
 *  Created on: May 18, 2021
 *      Author: chungnt@epi-tech.com.vn
*/
/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "Timer.h"

/**
  * @brief TM initialization function.
  * @param[in] Non.
  * @retval Non.
  */
void TM_Init(void)
{

/******************** work mode select ********************/
	_tm1 = 1; _tm0 = 0;	// Select TM timer/counter Mode
		
/**************** end of work mode select ****************/		
    _ton = 1;
/********************* clock select **********************/
	_ts = 1;	// Timer fTP clock source selection = 32kHz
	_tpsc2 = 0; _tpsc1 = 0; _tpsc0 = 0; // fTP/1 = 32kHz
		
/********************* end of clock select **********************/
	
	TM_CounterModeConfig(255-32); // count from (255-32) to 255 = 32 count	
	TM_EnableISR();
}

/**
  * @brief TM timer/counter mode period config function.
  * @param[in] period value,
  * overflow time=TempPeriod * Tclock,
  * @retval None.
  */
void TM_CounterModeConfig(u8 TempPeriod)
{
	_tmr = TempPeriod;
}

void TM_EnableISR(void)
{	
	_te = 1; // enable timer interrupt	
	_ton = 1; // on TM counter
	_emi  = 1; // enable global interrupt
}

