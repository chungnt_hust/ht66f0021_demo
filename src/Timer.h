/*
 * HT8_Timer.h
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
 *
 *      
*/

/* Define to prevent recursive inclusion -------------------------------------*/

#ifndef _PTM_H_
#define _PTM_H_

#include "HT66F0021.h"
#include "HT8_Type.h"


void TM_Init(void);
void TM_CounterModeConfig(u8 TempPeriod);
void TM_EnableISR(void);
#endif
/******************* (C) COPYRIGHT 2018 Holtek Semiconductor Inc *****END OF FILE****/