#include "HT66F0021.h"
#include "Interrupt.h"
#include "SystemClk.h"
#include "TimeBase.h"
#include "Timer.h"
#include "EmulatedEEPROM.h"

u8 eData[5] = {0};
bool flag1ms = 0;
u8 flag10ms = 0, flag100ms = 0, flag1s = 0;

void main()
{
	SysClock_Init();
	//TimeBase_Init();
	TM_Init();
	
	_pac7 = 0; // PA7 as output
	_pa7 = 1;
	_pac1 = 1; // PA1 as input
	_papu1 = 1; // PA1 pull up
	delayMs(500);
	
	//EmulatedEEPROM_Erase();
	//EmulatedEEPROM_Write_Byte(0x00, 'c');
	//EmulatedEEPROM_Write_Byte(0x01, 'h');
	//EmulatedEEPROM_Write_Byte(0x02, 'u');
	//EmulatedEEPROM_Write_Byte(0x03, 'n');
	//EmulatedEEPROM_Write_Byte(0x04, 'g');
	eData[0] = EmulatedEEPROM_Read_Byte(0x00);
	eData[1] = EmulatedEEPROM_Read_Byte(0x01);
	eData[2] = EmulatedEEPROM_Read_Byte(0x02);
	eData[3] = EmulatedEEPROM_Read_Byte(0x03);
	eData[4] = EmulatedEEPROM_Read_Byte(0x04);

	if((eData[0] == 'c') && (eData[1] == 'h') && (eData[2] == 'u')
	 	&& (eData[3] == 'n') && (eData[4] == 'g')) _pa7 = 0;
	else _pa7 = 1;
	
	while(1)
	{
		if(_pa1 == 0) _pa7 = 0;
		else _pa7 = 1;
		_pa7 = ~_pa7;
		delayMs(500);
//		if(flag1ms == 1) // set flag1ms trong ngat timer 1ms
//		{
//			flag1ms = 0;
//			flag10ms++;
//			
//		}
//		if(flag10ms >= 10)
//		{
//			flag10ms = 0;
//			flag100ms++;
//			
//		}
//		if(flag100ms >= 10)
//		{
//			flag100ms = 0;
//			flag1s++;
//			
//		}
//		if(flag1s >= 10)
//		{
//			flag1s = 0;
//			
//		}
	}
}